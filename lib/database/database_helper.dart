import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseHelper {
  DatabaseHelper() {
    _createDatabase();
  }
  late Database _database;
  static const String tableName = 'personalInformation';
  static const id = 'id';
  static const name = 'name';
  static const phone = 'phone';
  static const email = 'email';
  static const location = 'location';

  Future<Database> _createDatabase() async {
    var databaseFolderPath = await getDatabasesPath();
    String databasePath = join(databaseFolderPath,
        'information.db'); // to keep data in the folder path
    // join=> come from dependency, combine the folder path and database path
    // address.db is the name's database.
    _database = await openDatabase(databasePath);
    await _database.execute(
        'CREATE TABLE IF NOT EXISTS $tableName(id INTEGER PRIMARY KEY, name CHAR,phone CHAR, email CHAR, location CHAR) ');
    return _database;
  }

// add  data to the database
  Future<int> insertInformation(Map<String, dynamic> information) async {
    _database = await _createDatabase();
    return await _database.insert(tableName, information);
  }

  // get the alldata from the  database
  Future<List<Map<String, dynamic>>> getAllInformations() async {
    _database = await _createDatabase();
    return await _database.query(tableName,
        columns: ['id', 'name', 'phone', 'email', 'location']);
  }

  // to update the information, it's information has consist String and dynamic. firstly, when the update information  you need to check the id . therefore use <int>
  Future<int> updateInformation(Map<String, dynamic> information) async {
    _database = await _createDatabase();
    return await _database
        .update(tableName, information, where: 'id=?', whereArgs: [id]);
  }

  // to delete information database's the table
  Future<int> deleteInformation(int id) async {
    _database = await _createDatabase();
    return await _database.delete(tableName, where: 'id=?', whereArgs: [id]);
  }

  Future delete() async {
    _database = await _createDatabase();
    return await _database.rawDelete('DELETE FROM $tableName');
  }
}
