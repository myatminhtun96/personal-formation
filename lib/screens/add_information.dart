import 'package:flutter/material.dart';
import 'package:personal_information/database/database_helper.dart';
import 'package:personal_information/model/database_information.dart';

class AddInformation extends StatefulWidget {
  const AddInformation({super.key});

  @override
  State<AddInformation> createState() => _AddInformationState();
}

class _AddInformationState extends State<AddInformation> {
  GlobalKey<FormState> _key = GlobalKey();
  String? name, phone, email, location;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Add Information'),
        centerTitle: true,
      ),
      body: Form(
          key: _key,
          child: ListView(
            padding: const EdgeInsets.all(8),
            children: [
              TextFormField(
                //if the client forgot to input something data,for refill the data
                validator: (str) {
                  if (str == null || str.isEmpty) {
                    return 'Please refill your name';
                  } else {
                    return null;
                  }
                },
                // for save the data
                onSaved: (str) {
                  name = str;
                },
                decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    prefixIcon: Icon(Icons.person),
                    hintText: 'Enter your name',
                    hintStyle: TextStyle()),
              ),
              const SizedBox(
                height: 10,
              ),
              TextFormField(
                validator: (str) {
                  if (str == null || str.isEmpty) {
                    return 'Please refill your phone number';
                  } else {
                    return null;
                  }
                },
                onSaved: (str) {
                  phone = str;
                },
                decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    prefixIcon: Icon(Icons.phone),
                    hintText: 'Enter your phone number',
                    hintStyle: TextStyle()),
              ),
              const SizedBox(
                height: 10,
              ),
              TextFormField(
                validator: (str) {
                  if (str == null || str.isEmpty) {
                    return 'Please refill your emial';
                  } else {
                    return null;
                  }
                },
                onSaved: (str) {
                  email = str;
                },
                decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    prefixIcon: Icon(Icons.mail),
                    hintText: 'Enter your email',
                    hintStyle: TextStyle()),
              ),
              const SizedBox(
                height: 10,
              ),
              TextFormField(
                validator: (str) {
                  if (str == null || str.isEmpty) {
                    return 'Please refill your address';
                  } else {
                    return null;
                  }
                },
                onSaved: (str) {
                  location = str;
                },
                decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    prefixIcon: Icon(Icons.location_city),
                    hintText: 'Enter your address',
                    hintStyle: TextStyle()),
              ),
              const SizedBox(
                height: 10,
              ),
              ElevatedButton.icon(
                  onPressed: () async {
                    if (_key.currentState != null &&
                        _key.currentState!.validate()) {
                      _key.currentState?.save();
                      int id = await DatabaseHelper().insertInformation(
                          Information.insertInformation(
                              name: name ?? '',
                              phone: phone ?? '',
                              email: email ?? '',
                              location: location ?? ''));
                      print(id);
                      Navigator.pop(context, 'success');
                    }
                  },
                  icon: const Icon(Icons.save),
                  label: const Text('Save'))
            ],
          )),
    );
  }
}
