import 'package:flutter/material.dart';

import 'package:personal_information/database/database_helper.dart';
import 'package:personal_information/screens/add_information.dart';

class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Personal Information'),
        centerTitle: true,
      ),
      body: FutureBuilder<List<Map>>(
        future: DatabaseHelper().getAllInformations(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return ListView.builder(
              itemCount: snapshot.data?.length ?? 0,
              itemBuilder: (context, index) {
                Map? information = snapshot.data?[index];

                return Card(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(information?['id'].toString() ?? '')),
                      ListTile(
                        style: ListTileStyle.list,
                        leading: const Icon(Icons.person),
                        title: const Text('Name'),
                        subtitle: Text(information?['name']),
                      ),
                      ListTile(
                        style: ListTileStyle.list,
                        leading: const Icon(Icons.phone),
                        title: const Text('Phone'),
                        subtitle: Text(information?['phone']),
                      ),
                      ListTile(
                        leading: const Icon(
                          Icons.mail,
                        ),
                        title: const Text(
                          'Email',
                          selectionColor: Color.fromARGB(255, 255, 7, 48),
                        ),
                        subtitle: Text(information?['email']),
                      ),
                      ListTile(
                        style: ListTileStyle.list,
                        leading: const Icon(Icons.location_city),
                        title: const Text('Location'),
                        subtitle: Text(information?['location']),
                      ),
                    ],
                  ),
                );
              },
            );
          } else if (snapshot.hasError) {
            return Text(snapshot.error.toString());
          } else {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ),
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.add),
        onPressed: (() async {
          var result = await Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => const AddInformation(),
                  fullscreenDialog: true));
          if (result == 'success') {
            setState(() {});
          }
        }),
      ),
    );
  }
}
