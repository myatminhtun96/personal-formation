class Information {
  static Map<String, dynamic> insertInformation(
      {required String name,
      required phone,
      required String email,
      required String location}) {
    return {'name': name, 'phone': phone, 'email': email, 'location': location};
  }
}
